#!/bin/sh
# @vasm : vmodemset
# @level: root
# @description: Set modem device and PAP password
# 
# From comset on Vector Linux 4.0
#
# (c) Eko M. Budi, 2003
# (c) Vector Linux, 2003
#
# Released under GNU GPL

vdir=$(dirname $0)

. $vdir/vasm-functions

check_root

# Things we are going to set
MODEM=""
PPP_NAME=""
PPP_PASSWD=""

fpap="/etc/ppp/pap-secrets"
fchap="/etc/ppp/chap-secrets"

#######################################################################
# SCRIPT MAKER
mk_rc_serial() {
cat <<EOF
#!/bin/sh
#
# /etc/rc.d/rc.serial   `date '+%a %b %e %Y' 2>/dev/null`
#
# This rc.serial is started from: /etc/rc.d/rc.M
# This rc.serial file is created when you run: vmodemset
#
# /dev/ttyS0 = com1 ttyS1 = com2 ttyS2 = com3 ttyS3 = com4
#
# Try to autoconfigure modem device using setserial.

# If setserial is not there abort the configuration.
[ -x /sbin/setserial ] || exit 0

echo "Configuring modem serial port: /dev/$MODEM"

/sbin/setserial -v /dev/$MODEM auto_irq skip_test autoconfig session_lockout

# There have been multiple reports of late that the auto configuration
# of the serial ports is broken with some new motherboards, especially
# multi-processor ones.  For this reason, you have the option of telling
# setserial what irq, port, your modem is on.
# Note that we still need an automatic test to find out what uart we are 
# using.

# Tell setserial what irq, port, the modem in on if it can't detect your 
# modem automatically.
# Put # above for /sbin/setserial and remove # below for your modem device.

#/sbin/setserial -v /dev/ttyS0 irq 4 port 0x3F8 skip_test autoconfig session_lockout
#/sbin/setserial -v /dev/ttyS1 irq 3 port 0x2F8 skip_test autoconfig session_lockout
#/sbin/setserial -v /dev/ttyS2 irq 4 port 0x3E8 skip_test autoconfig session_lockout
#/sbin/setserial -v /dev/ttyS3 irq 3 port 0x2E8 skip_test autoconfig session_lockout

# End...
EOF
}

# test modem using ATZ
test_modem()
{
    rm -f /tmp/modem_test
    (cat $1 > /tmp/modem_test &) 2>/dev/null
    echo "ATZ" > $1
    sleep 2
    killall cat
    sleep 1
    cat /tmp/modem_test | grep -qe "OK"
    RESULT=$?
    rm /tmp/modem_test
    return $RESULT
}

detect_modem() {

  clear
  echo "Detecting modem ..." "Please wait"

  # If MODEM has been set, try it first
  if [ "$MODEM" ]; then
    if test_modem /dev/$MODEM; then
	return 0    
    fi
  fi
    
  # find classic serial
  for SIGNAL in "DSR" "CTS"; do
    LINE=`cat /proc/tty/driver/serial 2>/dev/null | grep -q "$SIGNAL"`
    if [ $LINE ]; then
      NUM=`echo $LINE | cut -b1`
      if test_modem /dev/ttyS${NUM}; then
	MODEM="ttyS${NUM}"
	mk_rc_serial > /etc/rc.d/rc.serial
        chmod +x /etc/rc.d/rc.serial
	return 0    
      fi
    fi
  done
  
  # Try WinModem
  for DEV in ttyLT0 ttySL0; do
    if [ -c /dev/$DEV ]; then
	if test_modem /dev/$DEV; then
	    MODEM="$DEV"
	    return 0    
	fi
    fi
  done

  # Check ;>FULLNAME<; Linux HW detection
  if [ -f /etc/sysconfig/;>LCNAME<; ]; then
    . /etc/sysconfig/;>LCNAME<;
    
    if [ "$MODEM_FULLNAME" ]; then
      $DCMD --backtitle "$BACKTITLE" --title "MODEM DETECTION" --msgbox \
"\nModem $MODEM_FULLNAME is detected.\n
However, it cannot be initialized. If this is a winmodem,
you need to get the driver from www.linmodem.org." 9 70
    fi
  else
    infobox "Sorry, no modem could be detected." "MODEM DETECTION"
    sleep 2
  fi
  return 1
}

# Check if modem has been linked
get_modem() {
  MODEM=`readlink /dev/modem`
  if [ "$MODEM" ]; then
    MODEM=`basename $MODEM`
  fi
}

set_pap() {
   [ -f $fpap ] && grep -ve "$PPP_NAME *\*" $fpap
   echo "$PPP_NAME * $PPP_PASSWD  ## "
}

## Still wrong, not tested !!!
set_chap() {
   ## Add CHAP password
   [ -f $fchap ] && grep -ve "$PPP_NAME *\*" $fchap | grep -ve "\* *$PPP_NAME "
   echo "$PPP_NAME * $PPP_PASSWD  ## " 
   echo "* $PPP_NAME $PPP_PASSWD  ## "
}


####################
# Set MODEM
menuA()
{
while [ 1 ]; do

TITLE="MODEM CONFIGURATION"
TEXT="\n
This configurator setup /dev/modem and creates a PPP account.\n
However, it works with hardware modems only. If you have a winmodem\n
like Lucent or most of laptop's internal modems, skip this menu.\n
Please go to http://www.linmodem.org to find the driver first.\n
For a hardware modem, select the port where it is connected."
DIMENSION="20 74 8"

get_modem

if [ $MODEM ]; then
   SKIP_MENU="Use the old $MODEM"
else
   SKIP_MENU="Do not set the modem"
fi

$DCMD --backtitle "$BACKTITLE" --title "$TITLE" --menu "$TEXT" $DIMENSION \
"SKIP" "$SKIP_MENU" \
"AUTO" "Try autodetect modem" \
"ttyS0" "COM1: under DOS" \
"ttyS1" "COM2: under DOS" \
"ttyS2" "COM3: under DOS" \
"ttyS3" "COM4: under DOS" \
"ttyS4" "COM5: under DOS" \
"ttyS5" "COM6: under DOS" \
"ttyS6" "COM7: under DOS" \
"ttyS7" "COM8: under DOS" \
"ttyS15" "PCTEL Modem"    \
"ttySHCF0" "HSF/HCF Modem"  \
"cuaHCF0"  "HSF/HCF Modem" \
2> $freply

  status=$?
  [ $status != 0 ] && return $status;
  
  reply=$(cat $freply)

  case $reply in
    AUTO)
      if detect_modem; then
        return 0
      fi
      ;;
    OLD|SKIP)
      [ $MODEM ] && return 0
      infobox "Skip modem configuration"
      sleep 2
      clean_exit 0      
      ;;
    *)
      MODEM=$reply
      (cd /dev; rm -f modem; ln -sf $MODEM modem)
      if [ -d /lib/udev/devices ]; then
        (cd /lib/udev/devices; rm -f modem; ln -sf $MODEM modem)
      fi
      mk_rc_serial > /etc/rc.d/rc.serial
      chmod 755 /etc/rc.d/rc.serial
      return 0
      ;;
  esac
done
}

###########################################################
# Set name for PPP
menuB() {
TITLE="SET PPP NAME"
TEXT="\n
OK, /dev/modem has been set up to $MODEM.
The next steps allow you to setup a PAP account for
PPP dialers like X-ISP. You may skip this if you 
will use GkDial or KPPP dialer.\n
Please type the PPP login name or empty to skip:"
DIMENSION="13 58"

  $WCMD --backtitle "$BACKTITLE" --title "$TITLE" \
  --inputbox "$TEXT" $DIMENSION $PPP_NAME 2> $freply

  status=$?
  [ $status != 0 ] && return $status;
  
  PPP_NAME=$(cat $freply)
  if [ -z "$PPP_NAME" ]; then
    infobox "Skipping PPP configuration."
    sleep 3
    clean_exit 0
  fi
  return 0  
}

menuC() {
TITLE="SET PPP PASSWORD"
TEXT="\n
Now type the PPP password for login name $PPP_NAME:"
DIMENSION="10 60"

  $WCMD --backtitle "$BACKTITLE" --title "$TITLE" \
  --passwordbox "$TEXT" $DIMENSION $PPP_PASSWD 2> $freply

  status=$?
  [ $status != 0 ] && return $status;
  
  PPP_PASSWD=$(cat $freply)
  # Security reason, delete it immediately
  rm -f $freply
  return 0  
}

menuD() {
  set_pap > $fpap
  chmod 600 $fpap
  #  set_chap > $fchap
  #  chmod 600 $fchap
  infobox "Modem and PPP password has been setup."
  sleep 3
  clean_exit 0
}

#######################
# Just for testing
testing()
{
  PPP_NAME="user"
  PPP_PASSWD="secret"
  set_pap > $fpap
  set_chap > $fchap
  cat $fpap
  echo ---
  cat $fchap

  echo =========
  PPP_NAME="user"
  PPP_PASSWD="secret2"
  set_pap > $fpap
  set_chap > $fchap
  cat $fpap
  echo ---
  cat $fchap

  echo =========
  PPP_NAME="user1"
  PPP_PASSWD="secret11"
  set_pap > $fpap
  set_chap > $fchap
  cat $fpap
  echo ---
  cat $fchap
}

############################
# MAIN

# testing
wizard menuA menuB menuC menuD


