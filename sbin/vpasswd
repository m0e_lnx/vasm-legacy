#!/bin/sh
# @vasm : vpasswd
# @level: root
# @description: change user or root password
# 
# (c) Eko M. Budi, 2003
# (c) Vector Linux, 2003
#
# Released under GNU GPL

vdir=$(dirname $0)

. $vdir/vasm-functions

pfile="/etc/passwd"

function list_user () {
  DIMENSION="12 60 8"
  TITLE="CHANGE PASSWORD"
  TEXT="\n
Select the user from the list then press OK,\n
or select EDIT if it is not in the list."

echo '$DCMD --backtitle "$BACKTITLE" --title "$TITLE" --menu "$TEXT" 20 50 10 \' > $fmenu
echo '"EDIT" "Enter name manually" \' >> $fmenu
## List the user, but only for the first 100
awk -F: '{ if ($3>=500) {print $0; num++}; if (num>=100) exit 0}' $pfile | sort | while read LINE; do
   N1=`echo $LINE | cut -d ':' -f 1`
   N2=`echo $LINE | cut -d ':' -f 5 | cut -d ',' -f 1`
   echo "$N1" "\"$N2 \" \\" >> $fmenu
done
echo '2> $freply' >> $fmenu

#cat $fmenu
while [ 1 ]; do
  source $fmenu

  result=$?
  [ $result != 0 ] && clean_exit $result

  USERNAME=`cat $freply`

  [ "$USERNAME" != "EDIT" ] && return 0
  
  USERNAME=""
  while [ -z "$USERNAME" ]; do
      inputbox "Enter user name" "CHANGE PASSWORD"
      result=$?
      [ $result != 0 ] && clean_exit $result
      USERNAME="`cat $freply`"
   done
   # check user existence
   if grep -qe "^${USERNAME}:" $pfile; then
       return 0
   fi
   retrybox "User '$USERNAME' does not exists; please choose another one"
done
}

function ask_pass() {
  dbug user=$USERNAME
  while [ 1 ]; do
    passwordbox "New password for $USERNAME" "CHANGE PASSWORD"
    result=$?
    [ $result != 0 ] && clean_exit $result

    USERPASS1="`cat $freply`"
    clean_tmp

    passwordbox "Re-enter new password" "CHANGE PASSWORD"
    result=$?
    [ $result != 0 ] && clean_exit $result

    USERPASS2="`cat $freply`"
    clean_tmp
  
    if [ ! "$USERPASS1" = "$USERPASS2" ]; then
      retrybox "Sorry. The passwords did not match." 
      continue
    fi
    $vdir/passwdx $USERNAME $USERPASS1 > /dev/null
    if [ $? = 0 ]; then 
      infobox "Changing password DONE "
      sleep 1
      clean_exit 0
    else
      retrybox "Changing password was FAILED. Let's repeat"
    fi  
  done
}


###############################################################
root_menu() {
  if [ "$USERNAME" ] && grep -qe "^${USERNAME}:" $pfile; then
     ask_pass
  else
    list_user
    ask_pass
  fi
}

###############################################################
user_menu() {

while [ 1 ]; do
  passwordbox "Old password of user $USERNAME" "CHANGE USER PASSWORD"
  result=$?
  [ $result != 0 ] && clean_exit $result
  OLDPASS="`cat $freply`"
  clean_tmp

  passwordbox "New password" "CHANGE USER PASSWORD"
  result=$?
  [ $result != 0 ] && clean_exit $result
  USERPASS1="`cat $freply`"
  clean_tmp

  passwordbox "Re-enter new password" "CHANGE USER PASSWORD"
  result=$?
  [ $result != 0 ] && clean_exit $result
  USERPASS2="`cat $freply`"
  clean_tmp
  
  if [ ! "$USERPASS1" = "$USERPASS2" ]; then
     retrybox "Sorry. The new passwords did not match. Please repeat"
     continue
  fi

  $vdir/passwdx1 $OLDPASS $USERPASS1

  case "$?" in
       0) 
          infobox "Changing password DONE " 
          sleep 1
	  clean_exit 0
	  ;;
       1)
          retrybox "Wrong old password"
	  ;;
       2)
          retrybox "New password is not strong enough"
	  ;;
   esac
done
}

###########################################################
# MAIN
if [ "$UID" = 0 ]; then
  USERNAME=$1
  root_menu
else
  USERNAME="$USER"
  user_menu
fi

clean_exit $?
